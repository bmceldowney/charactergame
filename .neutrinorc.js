const CordovaPlugin = require('webpack-cordova-plugin');

module.exports = (neutrino) => {
  /* make customizations */
  neutrino.config.entry('vendor')
    .add('react')
    .add('react-dom')
    .add('redux')

  neutrino.config.plugin('cordova')
    .use(CordovaPlugin, [{},{}])
};

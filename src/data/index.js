import backgrounds from './backgrounds'
import items from './items'
import skills from './skills'
import careers, { getExits } from './careers'
import mainMenu from './mainMenu'
import { portraits } from './portraits'
import { maleFirstNames, femaleFirstNames, lastNames } from './characterNames'

function dataObj(data) {
    data.toArray = function () {
        const array = []

        for (let key in this) {
            if (this.hasOwnProperty(key) && typeof this[key] !== 'function') {
                const item = this[key]
                item.key = key
                array.push(item)
            }
        }

        return array
    }

    return data
}

export default {
    backgrounds: dataObj(backgrounds),
    items: dataObj(items),
    skills: dataObj(skills),
    careers: dataObj(careers),
    getExits,
    mainMenu: dataObj(mainMenu),
    portraits: dataObj(portraits),
    names: {
        maleFirstNames,
        femaleFirstNames,
        lastNames
    }
}

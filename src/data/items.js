const items = {
    stick: {
        name: 'Stick'
    },
    lantern: {
        name: 'Lantern'
    },
    rope: {
        name: 'Rope'
    }
}

export default items

export const shopItems = [
    items.lantern
]

import skills from './skills'

const careers = {
    blacksmith: {
        name: 'Blacksmith',
        skills: [
            skills.smithing
        ],
        attributes: {
            luck: 0,
            strength: 5,
            skill: 1,
            stamina: 5,
            savvy: 0
        }
    },
    butcher: {
        name: 'Butcher',
        skills: [
            skills.cooking
        ],
        attributes: {
            luck: 0,
            strength: 3,
            skill: 3,
            stamina: 1,
            savvy: 0
        }
    },
    chiurgeon: {
        name: 'Chiurgeon',
        skills: [
            skills.surgery
        ],
        attributes: {
            luck: 1,
            strength: 1,
            skill: 5,
            stamina: 1,
            savvy: 1
        }
    },
    townWatchman: {
        name: 'Town Watchman',
        skills: [
            skills.brawling
        ],
        attributes: {
            luck: 0,
            strength: 3,
            skill: 1,
            stamina: 3,
            savvy: 1
        }
    },
    tailor: {
        name: 'Tailor',
        skills: [
            skills.mending
        ],
        attributes: {
            luck: 0,
            strength: 0,
            skill: 5,
            stamina: 0,
            savvy: 3
        }
    },
    cook: {
        name: 'Cook',
        skills: [
            skills.cooking
        ],
        attributes: {
            luck: 0,
            strength: 1,
            skill: 1,
            stamina: 1,
            savvy: 1
        }
    },
    recruit: {
        name: 'Recruit',
        skills: [
            skills.brawling
        ],
        attributes: {
            luck: 0,
            strength: 3,
            skill: 3,
            stamina: 3,
            savvy: 0
        }
    },
    soldier: {
        name: 'Soldier',
        skills: [
            skills.grappling
        ],
        attributes: {
            luck: 0,
            strength: 1,
            skill: 3,
            stamina: 3,
            savvy: 1
        }
    },
    veteran: {
        name: 'Veteran',
        skills: [
            skills.combatPlanning
        ],
        attributes: {
            luck: 1,
            strength: 1,
            skill: 1,
            stamina: 1,
            savvy: 1
        }
    },
    captain: {
        name: 'Captain',
        skills: [
            skills.command
        ],
        attributes: {
            luck: 0,
            strength: 0,
            skill: 3,
            stamina: 0,
            savvy: 3
        }
    },
    hunter: {
        name: 'Hunter',
        skills: [
            skills.hunting
        ],
        attributes: {
            luck: 0,
            strength: 1,
            skill: 3,
            stamina: 1,
            savvy: 0
        }
    },
    bandit: {
        name: 'Bandit',
        skills: [
            skills.ambush
        ],
        attributes: {
            luck: 0,
            strength: 1,
            skill: 3,
            stamina: 1,
            savvy: 3
        }
    },
    nobleheir: {
        name: 'Noble Heir',
        skills: [],
        attributes: {
            luck: 1,
            strength: 0,
            skill: 3,
            stamina: 0,
            savvy: 1
        }
    },
    courtier: {
        name: 'Courtier',
        skills: [
            skills.bluff
        ],
        attributes: {
            luck: 1,
            strength: 0,
            skill: 1,
            stamina: 0,
            savvy: 5
        }
    },
    hermit: {
        name: 'Hermit',
        skills: [
            skills.meditation
        ],
        attributes: {
            luck: 1,
            strength: 1,
            skill: 0,
            stamina: 3,
            savvy: 1
        }
    },
    student: {
        name: 'Student',
        skills: [
            skills.education
        ],
        attributes: {
            luck: 0,
            strength: 1,
            skill: 1,
            stamina: 1,
            savvy: 3
        }
    },
    clerk: {
        name: 'Clerk',
        skills: [
            skills.writing
        ],
        attributes: {
            luck: 0,
            strength: 0,
            skill: 1,
            stamina: 1,
            savvy: 3
        }
    },
    physician: {
        name: 'Physician',
        skills: [
            skills.healing
        ],
        attributes: {
            luck: 0,
            strength: 0,
            skill: 3,
            stamina: 0,
            savvy: 5
        }
    },
    professor: {
        name: 'Professor',
        skills: [
            skills.history
        ],
        attributes: {
            luck: 0,
            strength: 0,
            skill: 1,
            stamina: 1,
            savvy: 5
        }
    },
    alchemist: {
        name: 'Alchemist',
        skills: [
            skills.alchemy
        ],
        attributes: {
            luck: 0,
            strength: 0,
            skill: 3,
            stamina: 3,
            savvy: 1
        }
    },
    masteralchemist: {
        name: 'Master Alchemist',
        skills: [
            skills.advancedAlchemy
        ],
        attributes: {
            luck: 0,
            strength: 0,
            skill: 3,
            stamina: 1,
            savvy: 1
        }
    },
    vagabond: {
        name: 'Vagabond',
        skills: [
            skills.survival
        ],
        attributes: {
            luck: 0,
            strength: 1,
            skill: 0,
            stamina: 1,
            savvy: 3
        }
    },
    tinker: {
        name: 'Tinker',
        skills: [
            skills.persuasion
        ],
        attributes: {
            luck: 0,
            strength: 1,
            skill: 3,
            stamina: 1,
            savvy: 3
        }
    },
    laborer: {
        name: 'Laborer',
        skills: [],
        attributes: {
            luck: 0,
            strength: 3,
            skill: 1,
            stamina: 3,
            savvy: 0
        }
    },
    thief: {
        name: 'Thief',
        skills: [
            skills.stealth
        ],
        attributes: {
            luck: 0,
            strength: 3,
            skill: 1,
            stamina: 3,
            savvy: 0
        }
    }
}

export default careers

const exits = {
    'Blacksmith': [
        careers.blacksmith,
        careers.townWatchman,
        careers.tinker
    ],
    'Butcher': [
        careers.butcher,
        careers.physician,
        careers.cook,
        careers.hunter
    ],
    'Chiurgeon': [
        careers.chiurgeon,
        careers.physician,
        careers.professor,
        careers.alchemist
    ],
    'Town Watchman': [
        careers.townWatchman,
        careers.recruit,
        careers.thief,
        careers.bandit
    ],
    'Tailor': [
        careers.tailor,
        careers.recruit,
        careers.tinker
    ],
    'Cook': [
        careers.cook,
        careers.recruit,
        careers.butcher
    ],
    'Recruit': [
        careers.townWatchman,
        careers.soldier
    ],
    'Soldier': [
        careers.soldier,
        careers.veteran
    ],
    'Veteran': [
        careers.veteran,
        careers.captain
    ],
    'Captain': [
        careers.captain,
        careers.professor,
        careers.courtier
    ],
    'Hunter': [
        careers.hunter,
        careers.butcher,
        careers.tailor
    ],
    'Bandit': [
        careers.bandit,
        careers.tinker
    ],
    'Noble Heir': [
        careers.courtier
    ],
    'Courtier': [
        careers.courtier,
        careers.clerk,
        careers.student
    ],
    'Hermit': [
        careers.hermit,
        careers.vagabond
    ],
    'Student': [
        careers.student,
        careers.physician,
        careers.professor,
        careers.alchemist,
        careers.clerk
    ],
    'Clerk': [
        careers.clerk,
        careers.student
    ],
    'Physician': [
        careers.physician,
        careers.chiurgeon
    ],
    'Professor': [
        careers.professor,
        careers.alchemist,
        careers.physician
    ],
    'Alchemist': [
        careers.alchemist,
        careers.masteralchemist
    ],
    'Master Alchemist': [
        careers.masteralchemist,
        careers.professor
    ],
    'Vagabond': [
        careers.vagabond,
        careers.bandit,
        careers.tinker
    ],
    'Tinker': [
        careers.tinker,
        careers.blacksmith,
        careers.tailor
    ],
    'Laborer': [
        careers.laborer,
        careers.student
    ],
    'Thief': [
        careers.thief,
        careers.bandit,
        careers.recruit
    ]
}


export function getExits (careerName) {
    return exits[careerName]
}

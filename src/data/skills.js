export default {
    pickpocket: {
        name: 'Pickpocket'
    },
    education: {
        name: 'Education'
    },
    hunting: {
        name: 'Hunting'
    },
    smithing: {
        name: 'Smithing'
    },
    brawling: {
        name: 'Brawling'
    },
    grappling: {
        name: 'Grappling'
    },
    combatPlanning: {
        name: 'Combat Planning'
    },
    command: {
        name: 'Command'
    },
    surgery: {
        name: 'Surgery'
    },
    mending: {
        name: 'Mending'
    },
    cooking: {
        name: 'Cooking'
    },
    bluff: {
        name: 'Bluff'
    },
    ambush: {
        name: 'Ambush'
    },
    meditation: {
        name: 'Meditation'
    },
    riding: {
        name: 'Riding'
    },
    writing: {
        name: 'Writing'
    },
    healing: {
        name: 'Healing'
    },
    history: {
        name: 'History'
    },
    alchemy: {
        name: 'Alchemy'
    },
    advancedAlchemy: {
        name: 'Advanced Alchemy'
    },
    survival: {
        name: 'Survival'
    },
    persuasion: {
        name: 'Persuasion'
    },
    stealth: {
        name: 'Stealth'
    }
}

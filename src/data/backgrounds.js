import skills from './skills'
import careers from './careers'

export default {
    poverty: {
        name: 'Poverty',
        baseHireCost: 0,
        startingStats: {
            luck: 20,
            skill: 20,
            stamina: 10,
            strength: 15,
            savvy: 25
        },
        startingItems: [
            'stick'
        ],
        startingSkills: [
            skills.pickpocket
        ],
        startingCareers: [
            careers.recruit,
            careers.hunter,
            careers.hermit,
            careers.vagabond,
            careers.laborer,
            careers.thief
        ],
        maxSiblings: 1
    },
    lower: {
        name: 'Lower class',
        baseHireCost: 1,
        startingStats: {
            luck: 15,
            skill: 15,
            stamina: 15,
            strength: 25,
            savvy: 20
        },
        startingItems: [
            'stick'
        ],
        startingSkills: [
            skills.education
        ],
        startingCareers: [
            careers.recruit,
            careers.hunter,
            careers.cook,
            careers.tailor,
            careers.hermit,
            careers.student,
            careers.vagabond,
            careers.laborer,
            careers.thief
        ],
        maxSiblings: 7
    },
    middle: {
        name: 'Middle class',
        baseHireCost: 3,
        startingStats: {
            luck: 10,
            skill: 20,
            stamina: 20,
            strength: 20,
            savvy: 20
        },
        startingItems: [
            'stick'
        ],
        startingSkills: [
            skills.education
        ],
        startingCareers: [
            careers.recruit,
            careers.hunter,
            careers.cook,
            careers.tailor,
            careers.butcher,
            careers.hermit,
            careers.student,
            careers.vagabond,
            careers.thief
        ],
        maxSiblings: 5
    },
    upper: {
        name: 'Upper class',
        baseHireCost: 5,
        startingStats: {
            luck: 10,
            skill: 25,
            stamina: 25,
            strength: 20,
            savvy: 10
        },
        startingItems: [
            'stick'
        ],
        startingSkills: [
            skills.education
        ],
        startingCareers: [
            careers.recruit,
            careers.hunter,
            careers.nobleheir,
            careers.courtier,
            careers.clerk,
            careers.student
        ],
        maxSiblings: 3
    }
}

import data from '../data'

const male = 'm'
const female = 'f'
const portraits = data.portraits.slice()
const CAREER_START_AGE = 18
const CAREER_YEARS = 37
const CAREER_CHANGE_INTERVAL = 5
let nextId = 0

export default {
    generateNewCharacter: () => {
        const character = {}

        const background = getBackground()
        character.stats = Object.assign({}, background.startingStats)
        const portrait = getPortrait()
        const name = getName(portrait.gender)

        character.background = background.name
        character.portrait = portrait.url
        character.gender = portrait.gender
        character.firstName = name.firstName
        character.lastName = name.lastName
        character.age = Math.floor(Math.random() * CAREER_YEARS) + CAREER_START_AGE
        const careers = getCareers(background.startingCareers, Math.floor((character.age - CAREER_START_AGE) / CAREER_CHANGE_INTERVAL))
        character.careers = careers.map(career => career.name)
        const skills = careers.reduce((accumulator, career) => {
          return accumulator.concat(career.skills)
        }, [])

        const attributeBonus = careers.reduce((accumulator, career) => {
          accumulator.luck += career.attributes.luck
          accumulator.strength += career.attributes.strength
          accumulator.skill += career.attributes.skill
          accumulator.stamina += career.attributes.stamina
          accumulator.savvy += career.attributes.savvy

          return accumulator
        }, {
          luck: 0,
          strength: 0,
          skill: 0,
          stamina: 0,
          savvy: 0
        })

        character.stats.luck += attributeBonus.luck
        character.stats.strength += attributeBonus.strength
        character.stats.skill += attributeBonus.skill
        character.stats.stamina += attributeBonus.stamina
        character.stats.savvy += attributeBonus.savvy

        character.skills = Array.from(new Set(skills))
        character.id = nextId++
        character.hireCost = getHireCost(background, attributeBonus, skills)
        character.monthlySalary = getMonthlySalary(character)

        return character
    }
}

function getBackground () {
  const backgrounds = data.backgrounds.toArray()
  const index = Math.floor(Math.random() * backgrounds.length)

  return backgrounds[index]
}

function getHireCost (background, attributeBonus, skills) {
  const bonusCost = attributeBonus.luck + attributeBonus.skill + attributeBonus.strength + attributeBonus.stamina + attributeBonus.savvy
  return background.baseHireCost + (bonusCost * 2) + (skills.length * 10)
}

function getMonthlySalary (character) {
    return Math.ceil((character.hireCost / 12) + 2)
}

function getCareers (startingCareers, careerCount) {
  const retVal = []
  const localStarters = startingCareers.slice()
  const prevFilter = (item) => {
    const dontRepeat = [
      'Recruit',
      'Noble Heir',
      'Student'
    ]

    return !dontRepeat.find(element => item.name === element)
  }

  let career = pickCareer(localStarters, localStarters)
  retVal.push(career)

  for (var i = 0; i < careerCount; i++) {
    const exits = data.getExits(career.name).filter(prevFilter)
    career = pickCareer(exits, localStarters.filter(prevFilter))
    retVal.push(career)
  }

  return retVal
}

function pickCareer (careers, backupCareers) {
  let index = Math.floor(Math.random() * careers.length)
  let career = careers[index]

  if (Math.random() < 0.1) {
    index = Math.floor(Math.random() * backupCareers.length)
    career = backupCareers[index]
  }

  return career
}

function getPortrait () {
  const index = Math.floor(Math.random() * portraits.length)
  const portrait = portraits[index]
  portraits.splice(index, 1)

  return {
    url: `static/images/0${portrait.index}.png`,
    gender: portrait.gender
  }
}

function getName (gender) {
  const retVal = {}
  const names = data.names
  let firstNameIndex = 0

  if (gender === male) {
    firstNameIndex = Math.floor(Math.random() * names.maleFirstNames.length)
    retVal.firstName = names.maleFirstNames[firstNameIndex]
  } else {
    firstNameIndex = Math.floor(Math.random() * names.femaleFirstNames.length)
    retVal.firstName = names.femaleFirstNames[firstNameIndex]
  }

  const lastNameIndex = Math.floor(Math.random() * names.lastNames.length)
  retVal.lastName = names.lastNames[lastNameIndex]

  return retVal
}

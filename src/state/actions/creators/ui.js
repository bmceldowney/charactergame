import {
    SELECT_SECTION
} from '../types'

export function selectSection (sectionId) {
    return {
        type: SELECT_SECTION,
        sectionId
    }
}

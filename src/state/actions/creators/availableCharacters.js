import {
    POPULATE_AVAILABLE_CHARACTERS,
    HIRE_AVAILABLE_CHARACTER
} from '../types'

export function populateAvailableCharacters () {
    return {
        type: POPULATE_AVAILABLE_CHARACTERS
    }
}

export function hireAvailableCharacter (index, hireCost) {
    return {
        type: HIRE_AVAILABLE_CHARACTER,
        index,
        hireCost
    }
}

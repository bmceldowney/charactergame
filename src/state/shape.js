var shape = {
    game: {
        availableCharacters: [0, 1, 2],
        hiredCharacters: [3, 4],
        gold: 0
    },
    entities: {
        characters: {
            byId: {
                0: {
                    firstName: 'Joneson',
                    lastName: 'Flenticular',
                    background: 'lower',
                    luck: 40,
                    skill: 40,
                    stamina: 40,
                    strength: 40,
                    savvy: 40
                    inventory: [
                        'stick'
                    ],
                    career: {
                        current: 'child',
                        previous: [],
                        next: []
                    },
                    skills: [],
                }
            }
        },
    },
    ui: {
        currentSection: 'summary'
    }
}

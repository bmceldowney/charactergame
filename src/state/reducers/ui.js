import { SELECT_SECTION } from '../actions/types'
import { getMappedReducer } from './utils'

const initalState = {
    currentSection: 'summary'
}

function selectSection (state, action) {
    return Object.assign({}, state, { currentSection: action.sectionId })
}

const map = {
    SELECT_SECTION: selectSection
}

export default getMappedReducer(map, initalState)

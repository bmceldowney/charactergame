export function getMappedReducer (map, initalState) {
    return (state = initalState, action) => {
        const reducer = map[action.type]

        if (reducer) {
            return reducer(state, action)
        } else {
            return state
        }
    }
}

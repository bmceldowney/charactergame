import characterSystem from '../../systems/characterSystem'
import { POPULATE_AVAILABLE_CHARACTERS, HIRE_AVAILABLE_CHARACTER } from '../actions/types'
import { getMappedReducer } from './utils'

const initalState = {
    characters: [],
    availableCharacters: [],
    hiredCharacters: [],
    gold: 500
}

function populateAvailableCharacters (state, action) {
    const characterList = []
    if (state.availableCharacters.length === 0) {
        for (var i = 0; i < 6; i++) {
            const character = characterSystem.generateNewCharacter()
            state.availableCharacters.push(character.id)
            characterList.push(character)
        }

        return Object.assign({}, state, { characters: characterList })
    }

    return state
}

function hireAvailableCharacter (state, action) {
    const characterId = state.availableCharacters[action.index]
    const availableCharacters = state.availableCharacters.slice()
    const hiredCharacters = state.hiredCharacters.slice()

    hiredCharacters.push(characterId)
    availableCharacters.splice(action.index, 1)
    state.gold -= action.hireCost
    return Object.assign({}, state, { hiredCharacters, availableCharacters })
}

const map = {
    POPULATE_AVAILABLE_CHARACTERS: populateAvailableCharacters,
    HIRE_AVAILABLE_CHARACTER: hireAvailableCharacter
}

export default getMappedReducer(map, initalState)

import React from 'react'
import Header from '../../components/header'
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
  return {
    gold: state.game.gold
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    onItemSelected: (key) => {
      props.history.push(key)
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header)

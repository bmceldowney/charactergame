import React from 'react'
import css from './App.css'
import faCss from '../../static/font-awesome-4.7.0/css/font-awesome.min.css'
import data from '../../data'
import VisibleGameUi from '../VisibleGameUi'
import VisibleHeader from '../VisibleHeader'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

export default () => {
  return (
      <div>
        <VisibleHeader />
        <div className="app">
          <VisibleGameUi />
        </div>
      </div>
  )
}

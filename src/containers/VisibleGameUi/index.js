import React from 'react'
import css from './VisibleGameUi.css'
import data from '../../data'
import gameUi from '../../components/gameUi'
import { connect } from 'react-redux'
import { selectSection } from '../../state/actions'

const mapStateToProps = (state) => {
  return {
    menuItems: data.mainMenu.toArray(),
    currentSection: state.ui.currentSection,
    hiredCharacters: state.game.hiredCharacters.map(id => state.game.characters[id])
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    onItemSelected: (key) => {
      dispatch(selectSection(key))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(gameUi)

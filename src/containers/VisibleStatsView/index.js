import React from 'react'
import data from '../../data'
import CharacterSelector from '../../components/hireCharacterSelector'
import { hireAvailableCharacter } from '../../state/actions'
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
  return {
    characters: state.game.availableCharacters.map(characterId => state.game.characters[characterId])
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    onHireClicked: (index, hireCost) => {
      dispatch(hireAvailableCharacter(index, hireCost))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CharacterSelector)

import React from 'react'
import { render } from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import reducer from './state/reducers/index'
import { AppContainer } from 'react-hot-loader'
import App from './containers/App'
import { populateAvailableCharacters } from './state/actions'

const store = createStore(reducer)
const load = () => render((
  <AppContainer>
    <Provider store={store}>
      <App />
    </Provider>
  </AppContainer>
), document.getElementById('root'));

store.dispatch(populateAvailableCharacters())


if (module.hot) {
  module.hot.accept('./containers/App', load);
}

load();

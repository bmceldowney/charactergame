import React, { Component } from 'react'
import CharacterDetail from '../characterDetail'
import Carousel from 'react-slick'
import css from './hireCharacterSelector.css'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'


export default ({ characters,  onHireClicked }) => {
  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1
  };

  return (
    <Carousel className="character-picker" {...settings}>
      {getCharacterDetails(characters, onHireClicked)}
    </Carousel>

  )
}

function getCharacterDetails (characters, onHireClicked) {
  return characters.map((character, index) => {
    const hireCost = character.hireCost
    const hireClicked = () => onHireClicked(index, hireCost)

    return (<div className='container' key={index}>
      <div className='character-detail'>
        <div className='character-detail-inner'>
          <CharacterDetail character={character} />
        </div>
      </div>
      <div className="character-detail-buttons">
        <button className='button hire-button' onClick={hireClicked}>Hire {hireCost}</button>
      </div>
    </div>)
  })
}

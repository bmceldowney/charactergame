import React from 'react'
import css from './header.css'

export default ({ gold }) => {
  return (
    <div className="header">
      <div className="gold-value">{gold}</div>
      <div className="gold-icon"></div>
    </div>
  )
}

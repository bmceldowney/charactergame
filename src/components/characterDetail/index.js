import React from 'react'
import css from './characterDetail.css'

export default ({ character }) => {
    if (!character) {
        return <div></div>
    }

    const stats = character.stats
    const style = {
      backgroundImage: `url(${character.portrait})`,
      backgroundSize: 'contain'
    }
    const skills = character.skills.map(skill => skill.name).sort().map(skill => <div key={skill}>{skill}</div>)
    const career = character.careers[character.careers.length - 1]

    return (
        <div className="character-detail-container">
          <div className="character-detail-row">
            <div className="character-detail-column">
              <div className="character-detail-portrait" style={style}></div>

              <div className="stats-view">
                <div className="stat">
                  <label htmlFor="skill-value">Skill:</label>
                  <div id="skill-value">{obscureStat(stats.skill)}</div>
                </div>

                <div className="stat">
                  <label htmlFor="stamina-value">Stamina:</label>
                  <div id="stamina-value">{obscureStat(stats.stamina)}</div>
                </div>

                <div className="stat">
                  <label htmlFor="strength-value">Strength:</label>
                  <div id="strength-value">{obscureStat(stats.strength)}</div>
                </div>

                <div className="stat">
                  <label htmlFor="savvy-value">Savvy:</label>
                  <div id="savvy-value">{obscureStat(stats.savvy)}</div>
                </div>
              </div>
            </div>

            <div className="character-detail-column">
              <div className="character-detail-name"><span>{character.firstName}</span> <span>{character.lastName}</span></div>
              <div className="character-detail-career">{career}</div>
              <div className="character-detail-career">{character.age} years old</div>
              <br />
              <div>Skills</div>
              <div className="character-detail-career">{skills}</div>
            </div>
          </div>
        </div>
    )
}

function obscureStat(value) {
  return value
  switch(true) {
    case value < 30:
      return "Low"
    case value < 60:
      return "Decent"
    case value < 90:
      return "High"
    default:
      return "Super Human"
  }
}

import React from 'react'
import css from './gameUi.css'
import VisibleStatsView from '../../containers/VisibleStatsView'

export default ({ menuItems, currentSection, hiredCharacters, onItemSelected }) => {

  const selectItem = (evt) => {
    onItemSelected(evt.target.value)
  }

  const characters = hiredCharacters.map((character, index) => <div key={index}>{character.firstName} {character.lastName}</div>)
  const buttons = menuItems.map(item => <button className="button menu-button" onClick={selectItem} value={item.key} key={item.key}>{item.name}</button>)
  const section = getSection(currentSection, characters)

  return (
    <div className="menu-view view-container">
      <div className="content">
        {section}
      </div>
      <div className="button-container">{buttons}</div>
    </div>
  )
}

function getSection (key, characters) {
  switch (key) {
    case 'summary':
      return (<div>
              <div className='hired-characters' >
                <div>Hired characters:</div>
                <hr />
                {characters}
              </div>
            </div>)
    case 'hire':
      return <VisibleStatsView />
    default:
      return <div>Whoops!</div>
  }
}
